# CausalForestITE

This repository provides template code for fitting a causal forest model, calibrating predictions, plotting variable importance, and creating partial dependence plots. This code was used in the publication:  
 Seitz et al. Individualized Treatment Effects of Bougie vs Stylet for Tracheal Intubation in Critical Illness. Am J Respir Crit Care Med. 2023 Mar 6. doi: 10.1164/rccm.202209-1799OC.   
 https://pubmed.ncbi.nlm.nih.gov/36877594/

## Dependencies 
R version 3.6.6  
grf_1.2.0  
ggplot2_3.3.2  
dplyr_1.0.2  
boot_1.3.24  

## References
The code in this repository is based on the following paper:
Athey S, Wager S. Estimating Treatment Effects with Causal Forests: An Application. 2019;doi:10.48550/ARXIV.1902.07409.
Please cite this paper if you use this code in your research.

## Usage
To use this code, follow these steps:  
1.) Clone or download the repository to your local machine.  
2.) Install the required dependencies.  
3.) Open the R script and modify the data loading and preprocessing code to fit your data.  
4.) Run the code to fit the causal forest model and generate the plots.  




